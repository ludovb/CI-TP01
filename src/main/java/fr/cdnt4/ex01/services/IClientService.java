package fr.cdnt4.ex01.services;

import fr.cdnt4.ex01.models.Client;

import java.util.List;

public interface IClientService {

    List<Client> getClients();
}
