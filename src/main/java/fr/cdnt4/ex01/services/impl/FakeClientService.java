package fr.cdnt4.ex01.services.impl;

import fr.cdnt4.ex01.models.Client;
import fr.cdnt4.ex01.services.IClientService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FakeClientService implements IClientService {

    private List<Client> clientsFixture = new ArrayList<>();

    public FakeClientService() {
        clientsFixture.add(new Client("Michel P."));
        clientsFixture.add(new Client("Marcel P."));
        clientsFixture.add(new Client("Vlad D."));
        clientsFixture.add(new Client("Jessica R."));
        clientsFixture.add(new Client("Carlos S."));
    }

    @Override
    public List<Client> getClients() {
        return clientsFixture;
    }
}
