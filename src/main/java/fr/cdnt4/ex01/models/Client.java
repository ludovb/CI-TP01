package fr.cdnt4.ex01.models;

import java.util.UUID;

public class Client {

    private String guid;
    private String name;

    public Client(String name) {
        guid = UUID.randomUUID().toString();
        this.name = name;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
